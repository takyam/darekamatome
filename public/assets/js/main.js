(function ($) {
    $(function () {
        var $boards_containers = $('.boards-container');
        var $scroll_target = $('html, body');
        var $threads = $('#threads-list');

        var scroll_to_top = function(){
            $scroll_target.animate({scrollTop: 0}, 'fast');
        }

        $('.category-title').on('click', function () {
            $boards_containers.addClass('hide');
            $($(this).attr('href')).removeClass('hide');
            scroll_to_top();
            return false;
        });
        $('.board-subjects-link').on('click', function(){
            var $link = $(this);
            var link = $link.attr('data-url');
            $.ajax({
                url: '/main/subjects/'+$link.attr('data-id'),
                dataType: 'json',
                type: 'get',
                success: function(data){
                    var html = '';
                    for(var i = 0; i < data.length; i++){
                        html += '<li><a href="#'+data[i].number+'">'+data[i].name+' ('+data[i].count+')</a></li>';
                    }
                    $threads.empty().html(html);
                    scroll_to_top();
                },
                error: function(xhr){
                    alert('get error');
                }
            })
        });
    });
})(jQuery);