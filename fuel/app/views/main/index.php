<div class="row-fluid">
	<div class="span2">
		<ul class="nav nav-tabs nav-stacked">
			<?php foreach ($categories as $category): ?>
			<?php
				if (count($category->boards) <= 0){
					continue;
				}
			?>
			<li>
				<a href="#category-<?php echo $category->id; ?>" class="category-title">
					<?php echo $category->name; ?>
				</a>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
	<div class="span2">
		<?php foreach ($categories as $category): ?>
		<?php
		if (count($category->boards) <= 0){
			continue;
		}
		?>
		<ul class="nav nav-tabs nav-stacked hide boards-container" id="category-<?php echo $category->id; ?>">
			<?php foreach ($category->boards as $board): ?>
			<li>
				<a href="#" data-id="<?php echo $board->id; ?>" class="board-subjects-link">
					<?php echo $board->name; ?>
				</a>
			</li>
			<?php endforeach; ?>
		</ul>
		<?php endforeach; ?>
	</div>
	<div class="span8" id="threads">
		<ul class="nav nav-tabs nav-stacked" id="threads-list">
		</ul>
	</div>
</div>