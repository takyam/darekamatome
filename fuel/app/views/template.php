<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>darekamatome</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<?php echo Asset::css('bootstrap.min.css', 'bootstrap-responsive.min.css'); ?>
	<style type="text/css">
		body {
			padding-top: 60px;
			padding-bottom: 40px;
		}
	</style>
	<!--[if lt IE 9]>
	<script src="../assets/js/html5shiv.js"></script>
	<![endif]-->
</head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="brand" href="#">Darekamatome</a>

			<div class="nav-collapse collapse">
				<ul class="nav pull-right">
					<li><a href="#"><i class="icon-plus"></i> Add new matome</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<?php echo $content; ?>
		</div>
	</div>
	<hr>
	<footer>
		<p>Copyright &copy; 2013 <a href="http://twitter.com/takyam" target="_blank">@takyam</a>. All rights reserved. </p>
	</footer>
</div>
<?php echo Asset::js(array('jquery.js', 'bootstrap.min.js', 'main.js')) ?>
</body>
</html>
