<?php

namespace Fuel\Migrations;

class Add_sort_order_to_boards
{
	public function up()
	{
		\DBUtil::add_fields('boards', array(
			'sort_order' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
		));
	}

	public function down()
	{
		\DBUtil::drop_fields('boards', array(
			'sort_order'
		));
	}
}