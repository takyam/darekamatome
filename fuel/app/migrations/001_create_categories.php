<?php

namespace Fuel\Migrations;

class Create_categories
{
	private static $table_name = 'categories';

	public function up()
	{
		\DBUtil::create_table(static::$table_name, array(
			'id'          => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name'        => array('constraint' => 255, 'type' => 'varchar'),
			'deleted_at'  => array('type' => 'datetime', 'null' => true),
			'created_at'  => array('type' => 'datetime'),
			'updated_at'  => array('type' => 'datetime'),

		),  array('id'), true, 'mroonga COMMENT = \'engine "InnoDB"\'', 'utf8_general_ci');

		\DBUtil::create_index(static::$table_name, array('name'), 'idx_unique_category_name');
	}

	public function down()
	{
		\DBUtil::drop_table(static::$table_name);
	}
}