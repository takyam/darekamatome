<?php

namespace Fuel\Migrations;

class Add_sort_order_to_categories
{
	public function up()
	{
		\DBUtil::add_fields('categories', array(
			'sort_order' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
		));
	}

	public function down()
	{
		\DBUtil::drop_fields('categories', array(
			'sort_order'
		));
	}
}