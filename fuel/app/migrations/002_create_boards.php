<?php

namespace Fuel\Migrations;

class Create_boards
{
	private static $table_name = 'boards';

	public function up()
	{
		\DBUtil::create_table(static::$table_name, array(
			'id'          => array('constraint' => 11,  'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name'        => array('constraint' => 255, 'type' => 'varchar'),
			'server'      => array('constraint' => 255, 'type' => 'varchar'),
			'board_name'  => array('constraint' => 255, 'type' => 'varchar'),
			'category_id' => array('constraint' => 11,  'type' => 'int', 'unsigned' => true),
			'deleted_at'  => array('type' => 'datetime', 'null' => true),
			'created_at'  => array('type' => 'datetime'),
			'updated_at'  => array('type' => 'datetime'),

		),  array('id'), true, 'mroonga COMMENT = \'engine "InnoDB"\'', 'utf8_general_ci');

		\DBUtil::create_index(static::$table_name, array('server', 'board_name'), 'idx_unique_board');

		\DBUtil::add_foreign_key(static::$table_name, array(
			'key'       => 'category_id',
			'reference' => array(
				'table'  => 'categories',
				'column' => 'id',
			),
			'on_update' => 'NO ACTION',
			'on_delete' => 'NO ACTION',
		));
	}

	public function down()
	{
		\DBUtil::drop_table(static::$table_name);
	}
}