<?php

namespace Fuel\Migrations;

class Create_threads
{
	private static $table_name = 'threads';

	public function up()
	{
		\DBUtil::create_table(static::$table_name, array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'board_id' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'thread_number' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'file_name' => array('constraint' => 255, 'type' => 'varchar'),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'res_count' => array('constraint' => 11, 'type' => 'int', 'unsigned' => true),
			'deleted_at'  => array('type' => 'datetime', 'null' => true),
			'created_at'  => array('type' => 'datetime'),
			'updated_at'  => array('type' => 'datetime'),

		),  array('id'), true, 'mroonga COMMENT = \'engine "InnoDB"\'', 'utf8_general_ci');

		\DBUtil::create_index(static::$table_name, array('board_id', 'thread_number'), 'idx_unique_thread');
		\DBUtil::create_index(static::$table_name, array('name'), 'idx_thread_names');

		\DBUtil::add_foreign_key(static::$table_name, array(
			'key'       => 'board_id',
			'reference' => array(
				'table'  => 'boards',
				'column' => 'id',
			),
			'on_update' => 'NO ACTION',
			'on_delete' => 'NO ACTION',
		));
	}

	public function down()
	{
		\DBUtil::drop_table(static::$table_name);
	}
}