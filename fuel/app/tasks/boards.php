<?php
namespace Fuel\Tasks;
class Boards
{
	public static function run($speech = null)
	{
		return static::help();
	}

	public static function help()
	{
		return implode(PHP_EOL, array(
			'Update boards list :: $ oil r boards:update',
		));
	}

	public static function update()
	{
		return \Model_Board::refresh_boards_list();
	}
}