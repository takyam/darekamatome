<?php
class View_Main_Index extends ViewModel
{
	public function view()
	{
		$categories = Model_Category::find('all', array(
			'order_by' => array(
				array('sort_order', 'asc'),
			),
			'related' => array(
				'boards' => array(
					'order_by' => array(
						array('sort_order', 'asc'),
					),
				),
			),
		));

		$this->set('categories', $categories);
	}
}