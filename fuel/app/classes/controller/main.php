<?php
class Controller_Main extends Controller_Hybrid
{
	public function action_index(){
		$this->template->content = ViewModel::forge('main/index');
	}

	public function get_subjects($board_id){
		$board = Model_Board::find($board_id);
		if(is_null($board)){
			throw new HttpNotFoundException();
		}
		$this->response($board->get_subjects());
	}
}