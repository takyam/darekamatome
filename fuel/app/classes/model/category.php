<?php

class Model_Category extends \Orm\Model_Soft
{
	protected static $_has_many = array('boards');

	protected static $_properties = array(
		'id',
		'name',
		'sort_order' => array('default' => 0),
		'deleted_at',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);

	/**
	 * @param $category_name
	 * @return Model_Category
	 */
	public static function get_or_add_category($category_name)
	{
		$category = static::find('first', array('where' => array('name' => $category_name)));
		if (is_null($category)) {
			$category = Model_Category::forge();
			$category->name = $category_name;
			$category->save();
		}
		return $category;
	}

	public static function save_sort_orders(array $sort_orders)
	{
		$range = range(0, count($sort_orders) - 1);
		$orders = implode(',', $range);
		$names = '"' . implode('","', $sort_orders) . '"';
		$query = implode('', array(
			'UPDATE `categories` SET ',
			'sort_order = ELT(FIELD(`name`, ' . $names . '), ' . $orders . ') ',
			'WHERE `name` IN (' . $names . ')'
		));
		DB::query($query)->execute();
	}
}
