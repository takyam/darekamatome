<?php

class Model_Thread extends \Orm\Model
{
	protected static $_belongs_to = array('board');

	protected static $_properties = array(
		'id',
		'thread_number',
		'file_name',
		'name',
		'res_count' => array('default' => 0),
		'sort_order' => array('default' => 0),
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
}
