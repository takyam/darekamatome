<?php

class Model_Board extends \Orm\Model_Soft
{
	protected static $_belongs_to = array('category');
	protected static $_has_many = array('threads');

	protected static $_properties = array(
		'id',
		'name',
		'server',
		'board_name',
		'category_id',
		'sort_order' => array('default' => 0),
		'deleted_at',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);

	/**
	 * @param $server
	 * @param $board_name
	 * @return array|object
	 */
	public static function get_by_server_and_board_name($server, $board_name)
	{
		return static::find('first', array(
			'where' => array(
				array('server', $server),
				array('board_name', $board_name),
			),
			'order_by' => array(
				'id' => 'asc',
			),
		));
	}

	public static function refresh_boards_list()
	{
		//メニューを取得
		Config::load('2ch', true);
		$request = Request::forge(Config::get('2ch.bbsmenu_url', 'http://menu.2ch.net/bbsmenu.html'), 'curl');
		$request->set_method('GET');
		$response = $request->execute()->response();
		$html = $response->body;

		//HTMLをパース
		$document = phpQuery::newDocumentHTML($html);
		$board_links = $document->find('a[href*="2ch.net"][href^="http://"]');
		$ids = array();
		$category_names = array();
		$current_category_name = null;
		$order_count = 0;
		foreach ($board_links as $board_link) {
			$board_link = pq($board_link);
			$name = $board_link->text();
			if (empty($name)) {
				continue;
			}
			$href = $board_link->attr('href');
			if (!preg_match('/^https?:\/\/(\w+?)\.2ch\.net\/(\w+?)\//', $href, $matches)
				|| !array_key_exists(1, $matches) || empty($matches[1])
				|| !array_key_exists(2, $matches) || empty($matches[2])
			) {
				continue;
			}
			$server = $matches[1];
			$board_name = $matches[2];
			$board = Model_Board::get_by_server_and_board_name($server, $board_name);
			$category_name = $board_link->prevAll('b:first')->text();
			if ($category_name != $current_category_name) {
				$category_names[] = str_replace('"', '\\""', $category_name);
				$current_category_name = $category_name;
			}
			$category = Model_Category::get_or_add_category($category_name);
			if (is_null($board)) {
				$board = static::forge();
			}
			$board->server = $server;
			$board->board_name = $board_name;
			$board->name = $name;
			$board->category_id = $category->id;
			$board->sort_order = $order_count;
			$board->save();
			$ids[] = $board->id;
			$order_count++;
		}

		Model_Category::save_sort_orders($category_names);

		DB::query('DELETE FROM `boards` WHERE id NOT IN (' . implode(',', $ids) . ')')->execute();
	}

	public function get_subjects_url()
	{
		Config::load('2ch', true);
		return str_replace(
			array(':server', ':board'),
			array($this->server, $this->board_name),
			Config::get('2ch.board_url_base')
		) . Config::get('2ch.subject_file_name');
	}

	public function get_subjects(){
		$request = Request::forge($this->get_subjects_url(), 'curl');
		$request->set_method('GET');
		$response = $request->execute()->response();
		$text = $response->body;
		$text_encode = mb_detect_encoding($text, 'sjis-win, sjis, eucjp-win, eucjp, utf-8');
		$text = mb_convert_encoding($text, 'utf-8', $text_encode);
		$lines = explode("\n", preg_replace('/\r\n|\n\r|\r/', "\n", $text));
		$threads = array();
		foreach($lines as $line){
			preg_match('/^(.+)\.dat\<\>(.+)\s\(([0-9]+)\)/', $line, $matches);
			if(!is_array($matches) || count($matches) < 4){
				continue;
			}
			$threads[] = array(
				'number' => $matches[1],
				'name' => $matches[2],
				'count' => $matches[3],
			);
		}
		return $threads;
	}
}
